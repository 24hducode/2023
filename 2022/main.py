# main.py -- put your code here!

from stm32_vl53l0x import VL53L0X
from stm32_alphabot_v2 import AlphaBot_v2
from stm32_ssd1306 import SSD1306, SSD1306_I2C

bot = AlphaBot_v2()
print("Bonjour !")

bot.controlBuzzer(True)
print(bot.getOLEDaddr())

oled = SSD1306_I2C(128, 64, machine.I2C(1))
vl53l0x = VL53L0X(i2c=machine.I2C(1))

oled.text('Suivie de ligne', 0, 0)
oled.invert(True)
oled.show()

bot.TRSensors_calibrate()

    
while True: 
    if not bot.TRSensors_readLine(2) <= 300 and not bot.TRSensors_readLine(3) <= 300 and not bot.TRSensors_readLine(4) <= 300:
        bot.moveBackward(15) 
    if not bot. TRSensors_readLine(2) <= 300 and not bot.TRSensors_readLine(3) <= 300 and bot.TRSensors_readLine(4) <= 300:
        bot.setMotors(left=15)
        bot.setMotors (right=0)
    if not bot.TRSensors_readLine(2) <= 300 and bot.TRSensors_readLine(3) <= 300 and not bot.TRSensors_readLine(4) <= 300:
        bot.moveForward(15) 
    if not bot.TRSensors_readLine(2) <= 300 and bot.TRSensors_readLine(3) <= 300 and bot.TRSensors_readLine(4) <= 300:
        bot.setMotors(left=15)
        bot.setMotors(right=5)
    if bot.TRSensors_readLine(2) <= 300 and not bot.TRSensors_readLine(3) <= 300 and not bot. TRSensors_readLine(4) <= 300:
        bot.setMotors(left=0)
        bot.setMotors (right=15)
    if bot.TRSensors_readLine(2) <= 300 and not bot.TRSensors_readLine(3) <= 300 and bot.TRSensors_readLine(4) <= 300:
        bot.moveBackward(15)
    if bot.TRSensors_readLine(2) <= 300 and bot.TRSensors_readLine(3) <= 300 and not bot.TRSensors_readLine(4) <= 300:
        bot.setMotors(left=5)
        bot.setMotors(right=15)
    if bot. TRSensors_readLine(2) <= 300 and bot.TRSensors_readLine(3) <= 300 and bot.TRSensors_readLine(4) <= 300:
        bot.moveBackward(15)
    #oled.fill(0)
    #oled.show()
    if vl53l0x.getRangeMillimeters()/10 < 3:
        oled.text('Prout', 0, 20)
    else:
        oled.text('', 0, 20)
    oled.show()

