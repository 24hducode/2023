#!/usr/bin/python3
# To know COM port to use as argument, run following command before and after dongle connection:
# python -m serial.tools.list_ports
# Port in second result but not in first result is port used by dongle.

import sys
import time
import argparse
import random
from ComWithDongle import ComWithDongle


def randStr(length: int = 10):
	"""Generate a random string of characters
	:param length: number of characters"""
	allowedChar = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
	out = ''
	for _ in range(length):
		out += allowedChar[random.randrange(len(allowedChar))]
	return out


parser = argparse.ArgumentParser(description='Script to communicate with STM32WB55 dongle connected on computer')
parser.add_argument('-c', '--com', type=str, help='id of com port used')
args = parser.parse_args()

if not args.com:
	exit('missing argument --com')

com = ComWithDongle(com_port=args.com, recipient='CUSTOM', expect_peripheral_name='Morgator')
#com = ComWithDongle(com_port=args.com, recipient='ST', expect_peripheral_name='Morgator')
try:
	com.send({'type': 'testPcToCentral', 'msg': "Hello central, I'm PC"})
	print(com.get_com_message())
	nb_question_or_answer = 0
	msg_len = 1
	while nb_question_or_answer < 10000:
		com.connect_or_exit(retry=100)
		print("state", com.peripheral_state)
		if com.peripheral_state in ['ASK', 'ASK_CUSTOM']:
			# send message to peripheral
			com.send_question(msg=randStr(msg_len))
			msg_len += 1
			if msg_len > 520:
				msg_len = 20
			com.disconnect_peripheral()
			nb_question_or_answer += 1
		elif com.peripheral_state in ['RESULT', 'RESULT_CUSTOM']:
			# get message peripheral needs to send
			msg = com.get_com_message()
			print('answer', msg, flush=True)
			com.disconnect_peripheral()
			nb_question_or_answer += 1
		else:
			exit(f"bad state {com.peripheral_state}")
	# to activate this test, change range(7, 7) to another value, for example range(4, 7)
	for num in range(7, 7):
		toSend = {'type': 'testSizeSend'}
		for _ in range(num):
			toSend[randStr(10)] = randStr(50)
		com.send(toSend)
		com.wait_message()
	# to activate this test, change range(200, 200) to another value, for example range(200, 205)
	for valLength in range(200, 200):
		com.send({'type': 'testSizeRec', 'len_val': valLength})
		com.wait_message()
except KeyboardInterrupt:
	exit(0)
