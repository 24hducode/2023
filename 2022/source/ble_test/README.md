# BLE example
You can start from this example (inspired by https://github.com/micropython/micropython/tree/master/examples/bluetooth) to play with BLE.

Then you can create your own main_peripheral_xxx.py (to copy as main.py on peripheral) and main_pc_xxx.py (to run from PC).
You shall be able to keep other files unchanged.

For BLE, we use 2 roles: peripheral and central.
* Robot (or nucleo board) plays role of peripheral.
* Dongle is connected to PC and plays role of central.

Depending on the need of the peripheral, it will use a different state in BLE advertising:
* ASK: peripheral wants to get some data from central ST
* RESULT: peripheral wants to send some data to central ST
* ASK_CUSTOM: peripheral wants to get some data from central CUSTOM (central CUSTOM = YOUR PC TEAM)
* RESULT_CUSTOM: peripheral wants to send some data to central CUSTOM (central CUSTOM = YOUR PC TEAM)

Central regularly runs a scan to find BLE peripherals and when a peripheral is found in state ASK, RESULT, ASK_CUSTOM ou RESULT_CUSTOM.,
it connects on it to send or get data.
Once data is transferred, Central disconnects from peripheral.


1)  For peripheral part , in main_peripheral_xxx.py, you can define its name: drv = DrivePeripheral(name='Test2')
    We can also configure if we want to communicate with CENTRAL ST or CENTRAL CUSTOM.
            msg = drv.get_message(recipient='CUSTOM')
            msg = drv.get_message(recipient='ST')
            drv.send_message(recipient='CUSTOM', msg=”xxx”)
            drv.send_message(recipient='ST', msg=”xxx”)

Note : parameter « recipient » is mandatory, no default value.

2)  For PC part , in main_pc_xxx.py, we can configure rôle of central (ST ou CUSTOM) 
In option, we can specify peripheral name to connect =>  SHALL be USed by Your central to avoid connecting other peripherals that the expected one.
com = ComWithDongle(com_port=args.com, recipient='CUSTOM', expect_peripheral_name='Test2')
com = ComWithDongle(com_port=args.com, recipient='ST'') => SHALL BE ONLY USED BY CENTRAL ST TEAM 
When connected with recipient='CUSTOM' , we can know peripheral state with « com.peripheral_state », we can get ASK_CUSTOM ou RESULT_CUSTOM 
When connected with recipient='ST' , we can know peripheral state with « com.peripheral_state », we can get ASK ou RESULT 

Note : parameter « recipient » is mandatory, no default value.



# Installation for peripheral
Copy following files to peripheral.
* ble_advertising.py
* ble_peripheral.py
* main_peripheral_test.py (to rename as main.py)

# Installation on PC
Copy following files to PC.
* ComWithDongle.py
* main_pc_test.py

# Installation for Usb Dongle (part of central) !! ALREADY INSTALLED!
Copy following files to central.
* ble_advertising.py
* ble_central.py
* main_central.py (to rename as main.py)

## From PC, run
  > python main_pc_test.py --com <com port used by dongle>

To know COM port to use as argument, run following command before and after dongle connection:
>  python -m serial.tools.list_ports

# Notes
BLE routines are started as a background task, a ctrl-C will not stop BLE on peripheral, it can continue to receive messages and return answers. 