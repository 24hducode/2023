import time

from stm32_vl53l0x import VL53L0X
from stm32_alphabot_v2 import AlphaBot_v2
from stm32_ssd1306 import SSD1306, SSD1306_I2C
from ble_peripheral import DrivePeripheral

COEF = [0.02, 0.01, 0, -0.01, -0.02]
SPEED = 12
ROT_SPEED = 10
ROT_DELAY = 0.05
NAME = "Morgator"
COM = "COM9"
TOF_DIST = 5 # distance in cm

# Init
bot = AlphaBot_v2()
oled = SSD1306_I2C(128, 64, machine.I2C(1))
vl53l0x = VL53L0X(i2c=machine.I2C(1))
print(NAME)
oled.text(NAME, 0, 0)
oled.show()
bot.TRSensors_calibrate()

#Bluetooth
drv = DrivePeripheral(name=NAME)

def follow_line():
    dist = vl53l0x.getRangeMillimeters()/10
    while dist > TOF_DIST:
        sensors = bot.TRSensors_readLine(0)
        correct = 0
        for i in range(5):
            correct += sensors[i] * COEF[i]
        dist = vl53l0x.getRangeMillimeters()/10
        bot.setMotors(left=SPEED + correct, right = SPEED - correct)
    # Follow line Done
    bot.setMotors(left = 0, right = 0)

def rotate(deg):
    disp("Rotate : ", deg)
    if deg > 0: # Rotate right
        bot.setMotors(left = ROT_SPEED, right = -ROT_SPEED)
    else: # Rotate left
        bot.setMotors(left = -ROT_SPEED, right = ROT_SPEED)
    time.sleep(deg * ROT_DELAY)

def get_path():
    disp("Waiting JSON from ST")
    raw_path = drv.get_message(recipient='ST')
    # Send raw path to computer
    disp("JSON received\nSend it to PC")
    drv.send_message(recipient='CUSTOM', msg=raw_path)
    # Receive array of orientation (example : [90, -120, 90])
    disp("Waiting path from PC")
    path_array =  drv.drv.get_message(recipient='ST')
    disp("Received Path :", path_array)
    return path_array

def Enigme_Management():
    disp("Waiting Enigme from ST")
    enigme = drv.get_message(recipient='ST')
    # Send enigme to computer
    disp("Enigme received\nSend it to PC")
    drv.send_message(recipient='CUSTOM', msg=enigme)
    # Receive answere
    disp("Waiting answere from PC")
    answere =  drv.drv.get_message(recipient='ST')
    disp("Received answere :", answere)
    disp("Send answere to ST")
    drv.send_message(recipient='ST', msg=answere)
    disp("Answere send")

def disp(msg):
    # Send to terminal
    print(msg)
    # print on oled screen
    oled.text(msg, 0, 50) #TODO y pos
    oled.show()

def State_Machine():
    path_array = get_path()
    for orientation in path_array:
        rotate(orientation)
        follow_line()
        Enigme_Management()
    
