"""
Basic console log with colors and levels (info, error, warning, debug).

Example of use:
    import log
    log = log.Log(debug=True, name = __name__)
    log.i("This is an information message...")
    log.e("This is an error message...")
    log.w("This is a warning message...")
    log.d("This is a debug message...")

Note: we may use "logging" instead, see https://docs.python.org/3/howto/logging.html
      but it is nice to have less dependencies for micropython and "logging" is pretty big

"""

# For a color text:
# black 30, red 31, green 32, yellow 33, blue 34, magenta 35, cyan 36, white 37
# For a colored background:
# black 40, red 41, green 42, yellow 43, blue 44, magenta 45, cyan 46, white 47
# To remove the bold: replace the "1" by a "0" in "\033[1;34m"
# More colors and codes:
# https://www.lihaoyi.com/post/BuildyourownCommandLinewithANSIescapecodes.html


# TIPS: To remove all debug messages from all your modules, simply write in your main application:
#    import log
#    log.release_mode = True

release_mode = False

class console_colors:
    INFO    = "\033[1;34m"  # blue, bold
    ERROR   = "\033[1;31m"  # red, bold
    WARNING = "\033[1;33m"  # yellow, bold
    DEBUG   = "\033[1;35m"  # magenta, bold
    RESET   = "\033[0m"

class Log:
    def __init__(self, debug = False, name = None):
        self.debug = debug

        if name is not None:
            self.name = name
        else:
            # TODO does not work on micropython :-( please find a different way
            # avoid name = __name__ from the caller...
            import inspect
            from pathlib import Path
            self.name = Path(inspect.stack()[1].filename).stem

    def end_str(self):
        return self.name + ": " + console_colors.RESET

    def i(self, *args,**kwargs):
        print(console_colors.INFO + "INFO    " + self.end_str(), *args, **kwargs)

    def e(self, *args,**kwargs):
        print(console_colors.ERROR + "ERROR   " + self.end_str(), *args, **kwargs)

    def w(self, *args,**kwargs):
        print(console_colors.WARNING + "WARNING " + self.end_str(), *args, **kwargs)

    def d(self, *args,**kwargs):
        if self.debug and not release_mode:
            print(console_colors.DEBUG + "DEBUG   " + self.end_str(), *args, **kwargs)

# Unitary tests
# $> python3 log.py
if __name__ == "__main__":

    print("Let's go...")

    import log

    print("\n____debug=True")
    log = log.Log(debug=True)
    log.i("This is an information message.")
    log.e("This is an error message.")
    log.w("This is a warning message.")
    # Parameter tests
    log.w("This is a warning message", "with", "text parameters.")
    a = 1
    b = "great"
    log.w("This is a warning message with parameters:", a, b)
    log.d("This is a debug message (displayed because debug=True).")
    del log

    import log

    print("\n____debug=False")
    log = log.Log(debug=False, name="mymod_no_debug")
    log.i("This is an information message.")
    log.e("This is an error message.")
    log.w("This is a warning message.")
    log.d("This is a debug message (not displayed because debug=False).")
    print("Note: The debug message should not be there as debug=False")
    del log

    print('This is the End...')

