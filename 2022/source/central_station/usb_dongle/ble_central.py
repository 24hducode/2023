import bluetooth
import struct
import time
from ble_advertising import decode_services, decode_name, decode_manufacturer, decode_manufacturerID
from micropython import const
import _thread

# =================
_debug = 0
# ==================


# -----------------------------
# Function
# -----------------------------
def print_debug(level, *var_args):
    if _debug >= level:
        print(*var_args)


# ----------------------------------------
#  BLE Peripheral Management
# ----------------------------------------
_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE = const(3)
_IRQ_GATTS_READ_REQUEST = const(4)
_IRQ_SCAN_RESULT = const(5)
_IRQ_SCAN_DONE = const(6)
_IRQ_PERIPHERAL_CONNECT = const(7)
_IRQ_PERIPHERAL_DISCONNECT = const(8)
_IRQ_GATTC_SERVICE_RESULT = const(9)
_IRQ_GATTC_SERVICE_DONE = const(10)
_IRQ_GATTC_CHARACTERISTIC_RESULT = const(11)
_IRQ_GATTC_CHARACTERISTIC_DONE = const(12)
_IRQ_GATTC_DESCRIPTOR_RESULT = const(13)
_IRQ_GATTC_DESCRIPTOR_DONE = const(14)
_IRQ_GATTC_READ_RESULT = const(15)
_IRQ_GATTC_READ_DONE = const(16)
_IRQ_GATTC_WRITE_DONE = const(17)
_IRQ_GATTC_NOTIFY = const(18)
_IRQ_GATTC_INDICATE = const(19)
_IRQ_GATTS_INDICATE_DONE = const(20)
_IRQ_MTU_EXCHANGED = const(21)
_IRQ_L2CAP_ACCEPT = const(22)
_IRQ_L2CAP_CONNECT = const(23)
_IRQ_L2CAP_DISCONNECT = const(24)
_IRQ_L2CAP_RECV = const(25)
_IRQ_L2CAP_SEND_READY = const(26)
_IRQ_CONNECTION_UPDATE = const(27)
_IRQ_ENCRYPTION_UPDATE = const(28)
_IRQ_GET_SECRET = const(29)
_IRQ_SET_SECRET = const(30)

_FLAG_READ = const(0x0002)
_FLAG_WRITE_NO_RESPONSE = const(0x0004)
_FLAG_WRITE = const(0x0008)
_FLAG_NOTIFY = const(0x0010)
_FLAG_INDICATE = const(0x0020)

_ADV_IND         = const(0x00)
_ADV_DIRECT_IND  = const(0x01)
_ADV_SCAN_IND    = const(0x02)
_ADV_NONCONN_IND = const(0x03)

_UART_SERVICE_UUID = bluetooth.UUID("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
_UART_RX_CHAR_UUID = bluetooth.UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E")
_UART_TX_CHAR_UUID = bluetooth.UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E")

# list of states used by advertising
_24H_PROTOCOL_READY    = const(0x00)
_24H_PROTOCOL_ASK      = const(0x01)
_24H_PROTOCOL_RESULT   = const(0x02)
_24H_PROTOCOL_WAITING = const(0x03)
_24H_PROTOCOL_BUSY     = const(0x04)
_24H_PROTOCOL_ASK_CUSTOM = const(0x05)
_24H_PROTOCOL_RESULT_CUSTOM = const(0x06)
# list of states where dongle shall connect on peripheral, depending on recipient we use
connectable = {
    'ST': (_24H_PROTOCOL_ASK, _24H_PROTOCOL_RESULT),
    'CUSTOM': (_24H_PROTOCOL_ASK_CUSTOM, _24H_PROTOCOL_RESULT_CUSTOM)}

# Limit MTU to 256 because Windows negociate 256 max
_MAX_NB_BYTES = const(256)  # 256 (max MTU negociated by windows) Bytes
MAX_MTU = _MAX_NB_BYTES - 3  # 256 - 3 (header byte)


class BLECentralManagement:
    def __init__(self, ble):
        self._ble = ble
        self._ble.active(True)
        self._ble.irq(self._irq)
        self._reset()
        self.mtu = _MAX_NB_BYTES
        self.recipient = None
        self.scan_expect_peripheral_name = None
        self.event_type = None
        self.event_data = None
        self.written_msg_part = False
        self.written_complete_msg = False
        self.event_flag = _thread.allocate_lock()

    def _reset(self):
        # Cached name and address from a successful scan.
        self._name = None
        self._addr_type = None
        self._addr = None
        self._manufactory_state = None

        # Callbacks for completion of various operations.
        # These reset back to None after being invoked.
        self._scan_callback = None
        self._conn_callback = None

        # Connected device.
        self._conn_handle = None
        self._start_handle = None
        self._end_handle = None
        self._tx_handle = None
        self._rx_handle = None
        self.connected = 0

    def _irq(self, event, data):
        print_debug(3, "DEBUG: [IRQ], ", event)
        if event == _IRQ_SCAN_RESULT:
            addr_type, addr, adv_type, rssi, adv_data = data
            if adv_type in (_ADV_IND, _ADV_DIRECT_IND):
                manuf = decode_manufacturerID(adv_data)
                print_debug(2, "[IRQ] Potential manufacturer: 0x%04x" % manuf, "--", decode_name(adv_data))
                if manuf == 0x0030:  # STMicroelectronics hardware
                    manuf, manuf_state = decode_manufacturer(adv_data)
                    print_debug(2, "[IRQ] ST: STate=%x" % manuf_state)
                    if manuf_state in connectable[self.recipient]:
                        name = decode_name(adv_data) or "?"
                        if not self.scan_expect_peripheral_name or self.scan_expect_peripheral_name == name:
                            # Found a device, remember it and stop scanning.
                            self._addr_type = addr_type
                            self._addr = bytes(addr)  # Note: addr buffer is owned by caller so need to copy it.
                            self._name = name
                            self._manufactory_state = int(manuf_state)
                            self._ble.gap_scan(None)

        elif event == _IRQ_SCAN_DONE:
            self.event_type = 'scan_done'
            self.event_data = (self._addr_type, self._addr, self._name, self._manufactory_state)
            self.scan_expect_peripheral_name = None
            self.event_flag.release()

        elif event == _IRQ_PERIPHERAL_CONNECT:
            # Connect successful.
            conn_handle, addr_type, addr = data
            if addr_type == self._addr_type and addr == self._addr:
                self._conn_handle = conn_handle
                self._ble.gattc_discover_services(self._conn_handle)

        elif event == _IRQ_PERIPHERAL_DISCONNECT:
            # Disconnect (either initiated by us or the remote end).
            conn_handle, _, _ = data
            if conn_handle == self._conn_handle:
                # If it was initiated by us, it'll already be reset.
                self._reset()
            self.event_type = 'disconnected'
            self.event_flag.release()

        elif event == _IRQ_GATTC_SERVICE_RESULT:
            # Connected device returned a service.
            conn_handle, start_handle, end_handle, uuid = data
            #print_debug(2, "[IRQ] service", data)
            print_debug(1, "[IRQ] service", data)
            if conn_handle == self._conn_handle and uuid == _UART_SERVICE_UUID:
                self._start_handle, self._end_handle = start_handle, end_handle

        elif event == _IRQ_GATTC_SERVICE_DONE:
            # Service query complete.
            if self._start_handle and self._end_handle:
                print_debug(2, "[IRQ] Service discovered, ask for characteristics")
                self._ble.gattc_discover_characteristics(
                    self._conn_handle, self._start_handle, self._end_handle
                )
            else:
                print_debug(1, "[IRQ] Failed to find uart service.")

        elif event == _IRQ_GATTC_CHARACTERISTIC_RESULT:
            print_debug(2, "[IRQ] Characteristics....", data)
            # Connected device returned a characteristic.
            conn_handle, def_handle, value_handle, properties, uuid = data
            if conn_handle == self._conn_handle and uuid == _UART_RX_CHAR_UUID:
                self._tx_handle = value_handle
                print_debug(2, "[IRQ] Characteristics....set sending data from RX")
            if conn_handle == self._conn_handle and uuid == _UART_TX_CHAR_UUID:
                self._rx_handle = value_handle
                print_debug(2, "[IRQ] Characteristics....set reception of data from TX")

        elif event == _IRQ_GATTC_CHARACTERISTIC_DONE:
            print_debug(1, "[IRQ] Characteristics....DONE")
            # Characteristic query complete.
            if self._tx_handle is not None and self._rx_handle is not None:
                # We've finished connecting and discovering device, fire the connect callback.
                if self._conn_callback:
                    self._conn_callback()
                self.connected = 1
                self._ble.gattc_exchange_mtu(self._conn_handle)
            else:
                print("[IRQ] Failed to find uart rx characteristic.")

        elif event == _IRQ_GATTC_WRITE_DONE:
            conn_handle, value_handle, status = data
            if self.written_complete_msg:
                print_debug(1, "[IRQ] TX complete")
                self.event_type = 'write_done'
                self.written_complete_msg = False
                self.event_flag.release()
            else:
                self.written_msg_part = True

        elif event == _IRQ_GATTC_NOTIFY:
            conn_handle, value_handle, notify_data = data
            print_debug(1, "[IRQ] [RECEIVE] ", bytes(notify_data).decode('utf-8'))
            if conn_handle == self._conn_handle and value_handle == self._rx_handle:
                self.event_type = 'notify'
                self.event_data = bytes(notify_data)
                self.event_flag.release()

        elif event == _IRQ_GATTC_READ_RESULT:
            conn_handle, value_handle, char_data = data
            print_debug(1, "[IRQ] [READ RESULT] ", char_data)

        elif event == _IRQ_GATTC_READ_DONE:
            conn_handle, value_handle, status = data
            print_debug(1, "[IRQ] [READ DONE] ", status)

        elif event == _IRQ_MTU_EXCHANGED:
            # ATT MTU exchange complete (either initiated by us or the remote device).
            conn_handle, mtu = data
            print_debug(1, "[IRQ] [MTU EXCHANGE] ", mtu)
            self.mtu = mtu
            self.event_type = 'mtu_exchanged'
            self.event_flag.release()

    # Returns true if we've successfully connected and discovered characteristics.
    def is_connected(self):
        return (
            self._conn_handle is not None
            and self.connected == 1
            and self._tx_handle is not None
            and self._rx_handle is not None
        )

    # Find a device advertising the environmental sensor service.
    def scan(self, recipient, callback=None, expect_peripheral_name=None):
        self._addr_type = None
        self._addr = None
        self.recipient = recipient
        self.scan_expect_peripheral_name = expect_peripheral_name
        self._scan_callback = callback
        self._ble.gap_scan(2000, 30000, 30000)

    # Connect to the specified device (otherwise use cached address from a scan).
    def connect(self, callback=None):
        if self._manufactory_state is None:
            print("[ERROR][Connect] no manufactory state")
            return False
        self._ble.gap_connect(self._addr_type, self._addr)
        print_debug(1, "[CONNECT] Connected")
        return True

    # Disconnect from current device.
    def disconnect(self):
        if not self._conn_handle:
            return
        self._ble.gap_disconnect(self._conn_handle)
        self._reset()

    # Send data over the UART
    def write(self, data):
        if not self.is_connected():
            return
        print_debug(2, "[Write]:", data)
        data += "<END>"
        if len(data) > self.mtu - 3:
            chunk_size = self.mtu - 3
            for chunk in (data[i: i + chunk_size] for i in range(0, len(data), chunk_size)):
                self._ble.gattc_write(self._conn_handle, self._tx_handle, chunk, 1)
                while not self.written_msg_part:
                    time.sleep_ms(50)
                self.written_msg_part = False
        else:
            self._ble.gattc_write(self._conn_handle, self._tx_handle, data, 1)


# -------------------------------
#             MAIN
# -------------------------------
if __name__ == "__main__":
    ble = bluetooth.BLE()
    message_received = None
    message_complete = None
    central = BLECentralManagement(ble)
    peripheral_found = None

    def on_rx(data):
        dread = bytes(data)
        print_debug(2, "[On RX]: ", dread.decode('utf-8'))
        global message_received
        global message_complete
        if message_received is None:
            message_received = dread.decode('utf-8')
        else:
            message_received += dread.decode('utf-8')
        if message_received.find("<END>") > 0:
            print_debug(3, "--->[On RX] FOUND end")
            message_complete = 1

    def on_scan(addr_type, addr, name, state):
        global peripheral_found
        if addr_type is not None and state is not None:
            mac_address = ':'.join(['{:02x}'.format(addr[ele]) for ele in range(0, 6)])
            print_debug(1, "[On SCAN] Found peripheral:>", mac_address)
            peripheral_found = 1
            central.connect()
        else:
            print_debug(1, "[On SCAN] No peripheral found.")

    central.on_notify(on_rx)

    no_device_found = None
    central.scan(callback=on_scan)

    while True:

        if peripheral_found is None:

            # waiting full connection
            # TODO: put a TIMEOUT
            while not central.is_connected():
                time.sleep_ms(100)

            # peripheral are connected and central fully configured with periperal info
            print_debug(1, "STATE: ", central._manufactory_state)
            if central._manufactory_state == 1:  # ASK request
                time.sleep(2)
                # ??? Simulate sending sudoku
                central.write("XXXxxx"*45 + "<END>")
            elif central._manufactory_state == 2:  # receive result request
                # wait result from client
                message_received = None
                message_complete = None
                print("Waiting Result")
                # TODO: put a timeout
                while message_complete is None:
                    time.sleep_ms(200)
                print("Receive answer: \n", message_received)
            elif central._manufactory_state == 3:  # Waiting result
                central.write("You failed<END>")

            central.disconnect()
            print("Disconnected")

        time.sleep(4)
        no_device_found = None
        central.scan(callback=on_scan)
