import json

#entrée Json, à changé par la lecture de la TRAM du BLE par ST
jsonEntree =  '{"Chemins":[{"a":0,"b":4},{"a":1,"b":2},{"a":1,"b":4},{"a":1,"b":3},{"a":1,"b":6},{"a":2,"b":3},{"a":3,"b":4},{"a":3,"b":5},{"a":4,"b":6},{"a":5,"b":7}]}'

#traduction du Json en tableau pour Python
tabJson=json.loads(jsonEntree)

#tableau des valeurs triées, initialisé à 0'
tabOutput = [[0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0],[0, 0, 0, 0, 0, 0, 0, 0]]

#boucle des noeuds de 0 à 7
for i in range(8):
    #boucle du nombre de case du tableau
    for j in range(len(tabJson["Chemins"])):
        #SI la valeur du "a:" vaut la valeur du noeud actuel,
        if tabJson["Chemins"][j]['a'] == i:
           #dans le noeud actuel, à la position tableau [j]['b'], on ecrit 1
            tabOutput[i][tabJson["Chemins"][j]['b']] = 1

        #SI la valeur du "b:" vaut la valeur du noeud actuel,
        elif tabJson["Chemins"][j]['b'] == i:
            #dans le noeud actuel, à la position tableau [j]['a'], on ecrit 1
            tabOutput[i][tabJson["Chemins"][j]['a']] = 1

#le tableau est défini en deux dimensions,
# 1ere entrée -> noeud actuel
# 2eme entrée -> noeud possible où aller selon la première entrée   

#todelete
print("tableau de sortie")
print (tabOutput)
