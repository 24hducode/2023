import random

dico = {0:'Y',1:'B',2:'R',3:'G',4:'W',5:'O'}
code = []
code.append(random.choices(range(6), k=4))
code = code[0]

def simu_guess_code(code, solution):
    print(code)
    bien = 0
    mal = 0
    codetmp = list(code)
    solutiontmp = list(solution)
    for i,v in enumerate(solutiontmp):
      if v == codetmp[i]:
        bien += 1
        solutiontmp[i] = "#"
        codetmp[i] = "*"
    for i,v in enumerate(solutiontmp):
      if v in codetmp:
        mal += 1
        codetmp[codetmp.index(v)] = "*"
    return bien,mal

#Simule le renvoi des #,*...
def return_solution(bien, mal):
    toto = []
    for i in range(bien):
        toto.append('#')
    for i in range(mal):
        toto.append('*')
    while(len(toto)<4):
        toto.append('-')
    return toto

def translate(dico,code):
    solution = []
    for i in code:
        solution.append(dico[i])
    return solution


def Send_Responce(guess):
    guess_raw = guess.split(',')

    key_list = list(dico.keys())
    val_list = list(dico.values())
    guess = []
    for color in guess_raw:
        position = val_list.index(color)
        guess.append(key_list[position])
    correct, close = simu_guess_code(code, guess)
    responce = return_solution(correct, close)
    return ",".join(responce)