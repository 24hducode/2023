import matplotlib.pyplot as plt
import networkx as nx
from numpy import array
import json

#jsonEntree = '{"Chemins":[{"a":0,"b":2},{"a":0,"b":1},{"a":0,"b":3},{"a":1,"b":5},{"a":3,"b":5},{"a":7,"b":5},{"a":7,"b":6},{"a":7,"b":3},{"a":6,"b":2},{"a":6,"b":4},{"a":3,"b":4},{"a":2,"b":4},{"a":2,"b":3}]}'

def Save_img(jsonEntree):
    raw_json=json.loads(jsonEntree)["Chemins"]

    G = nx.Graph()
    # definition des noeuds
    G.add_node(0,label='0',col='orange')
    G.add_node(1,label='1',col='yellow')
    G.add_node(2,label='2',col='yellow')
    G.add_node(3,label='3',col='yellow')
    G.add_node(4,label='4',col='yellow')
    G.add_node(5,label='5',col='yellow')
    G.add_node(6,label='6',col='yellow')
    G.add_node(7,label='7',col='orange')

    for connexion in raw_json:
        G.add_edge(connexion["a"], connexion["b"],weight=1 ,style='solid')

    pos = nx.spring_layout(G)  
    labels_edges = {edge:'' for edge in G.edges}
    liste = list(G.nodes(data='label'))

    labels_nodes = {}
    for noeud in liste:
        labels_nodes[noeud[0]]=noeud[1]
    print(labels_nodes)
    liste = list(G.nodes(data='col'))
    colorNodes = {}
    for noeud in liste:
        colorNodes[noeud[0]]=noeud[1]
    colorList=[colorNodes[node] for node in colorNodes]
    #labels_nodes = {0: '0', 1: '1', 2: '2', 3: '4', 4: '5', 5: '6', 6: '7'}

    nx.draw_networkx_labels(G, pos, labels=labels_nodes,font_size=20,
                            font_color='black',
                            font_family='sans-serif')

    nx.draw_networkx_nodes(G, pos, node_size=700,alpha=0.9, node_color=colorList)
    nx.draw_networkx_edges(G, pos,width=1)
    nx.draw_networkx_edge_labels(G, pos, edge_labels=labels_edges)

    plt.axis('off')
    plt.savefig('path.png')