import json

#entrée Json, à changé par la lecture de la TRAM du BLE par ST
#jsonEntree =  '{"Chemins":[{"a":0,"b":4},{"a":1,"b":2},{"a":1,"b":4},{"a":1,"b":3},{"a":1,"b":6},{"a":2,"b":3},{"a":3,"b":4},{"a":3,"b":5},{"a":4,"b":6},{"a":5,"b":7}]}'

#jsonEntree = '{"Chemins":[{"a":0,"b":5},{"a":0,"b":1},{"a":0,"b":4},{"a":3,"b":1},{"a":7,"b":1},{"a":4,"b":7},{"a":6,"b":4},{"a":4,"b":2},{"a":2,"b":7}]}'
#jsonEntree = '{"Chemins":[{"a":0,"b":1},{"a":0,"b":2},{"a":0,"b":3},{"a":6,"b":0},{"a":7,"b":6},{"a":5,"b":7},{"a":7,"b":4}]}'
#jsonEntree = '{"Chemins":[{"a":0,"b":4},{"a":2,"b":4},{"a":0,"b":6},{"a":0,"b":1},{"a":5,"b":1},{"a":3,"b":1},{"a":7,"b":3}]}'
#jsonEntree = '{"Chemins":[{"a":0,"b":1},{"a":0,"b":2},{"a":0,"b":3},{"a":0,"b":4},{"a":0,"b":5},{"a":0,"b":6},{"a":7,"b":1},{"a":7,"b":2},{"a":7,"b":3},{"a":7,"b":4},{"a":7,"b":5},{"a":7,"b":6}]}'
jsonEntree = '{"Chemins":[{"a":0,"b":2},{"a":0,"b":1},{"a":0,"b":3},{"a":1,"b":5},{"a":3,"b":5},{"a":7,"b":5},{"a":7,"b":6},{"a":7,"b":3},{"a":6,"b":2},{"a":6,"b":4},{"a":3,"b":4},{"a":2,"b":4},{"a":2,"b":3}]}'
#traduction du Json en tableau pour Python


f = open('OrientationParcours.txt', 'r')
lines = f.readlines()
f.close()

orientation = []
for line in lines:
    line = line.replace("\n", "")
    line = line.replace(":", "")
    tab = line.split(" ")
    tab[0] = int(tab[0])
    tab[1] = int(tab[1])
    tab[2] = int(tab[2])
    tab[3] = tab[3]
    orientation.append(tab)

#print (orientation)



def Get_Path(jsonEntree):
    raw_json=json.loads(jsonEntree)["Chemins"]
    tree = {0:[], 1:[], 2:[], 3:[], 4:[], 5:[], 6:[], 7:[]}
    for connexion in raw_json:
        tree[connexion["a"]].append(connexion["b"])
        tree[connexion["b"]].append(connexion["a"])

    print(tree)

    path_A = [0]
    path_B = [7]

    B_Done = False
    A_Done = False
    revert_A = -2
    revert_B = -2
    while not A_Done or not B_Done:
        Update = False
        possibility_B = list(set(tree[path_B[-1]])-set(path_A)-set(path_B))

        if not A_Done and (len(possibility_B) > 1 or B_Done):
            possibility_A = list(set(tree[path_A[-1]])-set(path_A)-set(path_B))
            print("A at pos ", path_A[-1], ":", possibility_A)

            if len(possibility_A) == 0:
                path_A.append(path_A[revert_A])
                revert_A -=2
                Update = True
            elif len(possibility_A) == 1:
                path_A.append(possibility_A[0])
                Update = True
                revert_A =-2
            else:
                pos = list(set(possibility_A)-set(possibility_B))
                print("Pos : ", pos)
                if len(pos) == 0:
                    print("Possibilité commune")
                    path_A.append(possibility_A[0])
                else:
                    path_A.append(pos[0])
                Update = True
                revert_A =-2

        if len(set(path_A))==4:
            A_Done = True
        possibility_A = list(set(tree[path_A[-1]])-set(path_A)-set(path_B))

        if not B_Done:
            possibility_B = list(set(tree[path_B[-1]])-set(path_A)-set(path_B))
            print("B at pos ", path_B[-1], ":", possibility_B)

            if len(possibility_B) == 0:
                path_B.append(path_B[revert_B])
                revert_B -=2
                Update = True
            elif len(possibility_B) == 1:
                path_B.append(possibility_B[0])
                Update = True
                revert_B =-2
            else:
                pos = list(set(possibility_B)-set(possibility_A))
                print("Pos : ", pos)
                if len(pos) == 0:
                    print("Possibilité commune")
                    path_B.append(possibility_B[0])
                else:
                    path_B.append(pos[0])
                Update = True
                revert_B =-2

        if len(set(path_B))==4:
            B_Done = True

        #possiblity = list(set(tree[path_A[-1]]) - set(tree[path_B[-1]]))
        print("Path A :", path_A)
        print("Path B :", path_B)
        print("_____")
        if not Update:
            print(path_A)
            print(path_B)
            print("Error no path found")
            break

    print("Solution :")
    print(path_A)
    print(path_B)
    return path_A, path_B

def find_orientation(a, b, c):
    print(a,b,c)
    for l in orientation:
        if l[0] == a and l[1] == b and l[2] == c:
            return l[3]
    print("Not found")

def get_orientation(start, path):
    print(path)
    orientation = [find_orientation(start, start, path[1])]
    for i in range (1, len(path)-1):
        ori = find_orientation(path[i-1], path[i], path[i+1])
        orientation.append(ori)
    return orientation

def Path_Solver(raw_json):
    path_A, path_B = Get_Path(raw_json)

    orientation_A = get_orientation(0, path_A)
    orientation_B = get_orientation(7, path_B)
    print(orientation_A)
    print(orientation_B)
    return path_A, path_B, orientation_A, orientation_B

#Path_Solver(jsonEntree)