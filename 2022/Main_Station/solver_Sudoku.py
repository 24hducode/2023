#!/usr/bin/env python3
#-*- coding: utf-8 -*-

def display_grid(grid: list):
    print("---")
    for row in range(9):
        print(grid[row])

def is_valid_row(grid: list, digit: int, row: int) -> bool:
    return grid[row].count(digit) == 1

def is_valid_col(grid: list, digit: int, col: int) -> bool:
    return [grid[i][col] for i in range(9)].count(digit) == 1

# Squares are numbered from left to right, top to bottom:
# (0,0)...(2,0)
#   .   X   .
# (0,2)...(2,2)
def is_valid_square(grid: list, digit: int, square: tuple) -> bool:
    r = 3 * square[0]
    c = 3 * square[1]
    count = 0
    for row in range(r, r + 3):
        for col in range(c, c + 3):
            if grid[row][col] == digit:
                count += 1
    return count == 1

def is_valid_digit(grid: list, row: int, col: int) -> bool:
    digit = grid[row][col]
    return is_valid_row(grid, digit, row) and is_valid_col(grid, digit, col) and is_valid_square(grid, digit, (row // 3, col // 3))

def next_empty_cell(grid: list, current_row: int):
    for row in range(current_row, 9):
        for col in range(9):
            if grid[row][col] == 0:
                return (row, col)

    return None

def backtrack(grid: list, row: int, col: int) -> bool:

    for i in range(9):
        grid[row][col] += 1

        if is_valid_digit(grid, row, col):
            next_cell = next_empty_cell(grid, row)
            if next_cell is None or backtrack(grid, next_cell[0], next_cell[1]):
                return True

    # Reset & backtrack
    grid[row][col] = 0
    return False

def deserialize(raw: str) -> list:
    raw = raw[8:].replace(' ', '0').replace(',', ' ').strip()
    tab = list(map(int, raw.split(' ')))
    assert len(tab) == 81, "Invalid grid size"
    return [tab[i:i+9] for i in range(0, len(tab), 9)]

def serialize(grid: list) -> str:
    tab = []
    for i in range(9):
        tab.extend(grid[i])
    raw = "<SUDOKU>"+",".join(map(str, tab))
    assert len(tab) == 81, "Invalid grid size"
    return raw

def solve_sudoku(raw: str) -> str:
    grid = deserialize(raw)

    # display_grid(grid)
    # Assumes their is at least one empty cell in the the first row ¯\_(ツ)_/¯.
    cell = next_empty_cell(grid, 0)
    backtrack(grid, cell[0], cell[1])
    # display_grid(grid)

    return serialize(grid)


# Example from the subject => UNSOLVABLE...
#
#  ex = "<SUDOKU> , ,9, , ,2, , ,5,5,3,8, ,6,4, , ,9, ,1,6,2, , , , ,3, , , ,3, ,2,7, , , , ,5,4,6, , ,1, , , , ,7, ,1,5,3,4, ,3, , ,8, ,1,9, ,6,7, , ,3, , ,8,5, , ,9,1, , , ,4,7,"


# Uncomment for testing
#  ex1 = "<SUDOKU> , ,9, , ,2, , ,5,5,3,8, ,6,4, , ,9,1,6,2, , , , ,3, , , ,3, ,2,7, , , , ,5,4,6, , ,1, , , , ,7, ,1,5,3,4, ,3, , ,8, ,1,9, ,6,7, , ,3, , ,8,5, , ,9,1, , , ,4,7, ,"

#  ex2 = "<SUDOKU> ,5,8, ,4, ,2, ,6, , , ,3,6, , , , , ,1,4, , ,8, , , , , , , , ,5, , ,8, , ,2, , ,4, , ,9,5, , ,6, , ,3,4, ,2,6,3, ,9, , , , , ,4, , ,2, , , , ,8, , , , ,3, , , ,"

#  print(solve(ex1))
#  print(solve(ex2))

