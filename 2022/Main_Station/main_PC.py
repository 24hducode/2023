from ComWithDongle import ComWithDongle

import solver_Sudoku
import Path_Solver
import mastermind_ST


PRINTER = True
DOUBLE_BOT = True

FAKE_ST = False

if PRINTER:
    from Peripage import Peripage
    import Path_View
    from PIL import Image
    import textwrap
    try:
        printer = Peripage()
        printer.text.set_font(align= "center", fontsize = 40)
    except:
        print("Cant' connect to printer")
        PRINTER = False
        pass

COM_DONGLE = "COM9"

PATH = '{"Chemins":[{"a":0,"b":2},{"a":1,"b":5},{"a":2,"b":4},{"a":3,"b":4},{"a":3,"b":5},{"a":3,"b":7}, {"a":4,"b":6}, {"a":5,"b":7},{"a":6,"b":7}]}'

#PATH = '{"Chemins":[{"a":0,"b":2},{"a":0,"b":1},{"a":0,"b":3},{"a":1,"b":5},{"a":3,"b":5},{"a":7,"b":5},{"a":7,"b":6},{"a":7,"b":3},{"a":6,"b":2},{"a":6,"b":4},{"a":3,"b":4},{"a":2,"b":4},{"a":2,"b":3}]}'
SUDOKU = "<SUDOKU> ,5,8, ,4, ,2, ,6, , , ,3,6, , , , , ,1,4, , ,8, , , , , , , , ,5, , ,8, , ,2, , ,4, , ,9,5, , ,6, , ,3,4, ,2,6,3, ,9, , , , , ,4, , ,2, , , , ,8, , , , ,3, , , ,"

MUSIC = '<MYSTERE>D-4:1D-4:1D-4:1G-4:4D#5:4S-1:2C#5:1C-5:1A#4:1A#5:4D#5:2S-1:1C#5:1C-5:1A#4:1A#5:4D#5:2C#5:1C-5:1C#5:1A#4:5'
MASTERMIND = '<MASTERMIND>GUESS_INIT'

enigmes = [MUSIC, SUDOKU, MASTERMIND]
# Init
com = ComWithDongle(com_port=COM_DONGLE, recipient='CUSTOM', expect_peripheral_name='Morgator')

def send_to_bot(msg):
    com.connect_or_exit(retry=100)
    com.send_question(str(msg))
    com.disconnect_peripheral()

def receive_from_bot():
    com.connect_or_exit(retry=100)
    msg = com.get_com_message()['msg']
    com.disconnect_peripheral()
    return msg

def Solve_Path():
    #print("Wait raw path from Bot")
    #raw_path = receive_from_bot()
    raw_path = PATH
    print("Raw path from bot :", raw_path)
    path_A, path_B, orientation_A, orientation_B = Path_Solver.Path_Solver(raw_path)
    print("Send Orientation A :", orientation_A)
    send_to_bot(" ".join(orientation_A))
    if PRINTER:
        Path_View.Save_img(raw_path)
        with Image.open("path.png") as img:
            printer.print_image(img)
        printer.print_text("#### Robot A ####")
        printer.print_text("Chemin : ")
        printer.print_text(str(path_A))
        printer.print_text("Rotation : ")
        printer.print_text(str(orientation_A))
        printer.print_text("#### Robot B ####")
        printer.print_text("Chemin : ")
        printer.print_text(str(path_B))
        printer.print_text("Rotation : ")
        printer.print_text(str(orientation_B))
        printer.print_break(125)
    # send to bot B
    if DOUBLE_BOT:
        com.expect_peripheral_name = "seraphor"
        print("Send Orientation B :", orientation_B)
        send_to_bot(" ".join(orientation_B))  
        com.expect_peripheral_name = "Morgator"  

def Solve_Enigme():
    print("Wait Enigme from Bot")
    try:
        enigme = receive_from_bot()
    except:
        print("Wait")
    print("Enigme :", enigme)
    if "<SUDOKU>" in enigme:
        print("Sudoku")
        solution = solver_Sudoku.solve_sudoku(enigme)
        if PRINTER:
            printer.print_text("#### SUDOKU ####")
            printer.text.set_font(align= "center", fontsize = 40)
            solution_print = solution[8:].replace(',','|')
            txt = "|"+'\n|'.join(textwrap.wrap(solution_print, 18))+"|"
            print(txt)
            printer.print_text(txt)
            printer.print_break(125)
        print("Send Solution :", solution)
        send_to_bot(solution)
    else:
        print("Unknow Enigme")


#FAKE ST function
def send_Path():
    print("ST : Send Path to Bot")
    send_to_bot(PATH)

def send_Enigme():
    enigme = enigmes.pop(0)
    print("Send :", enigme)
    send_to_bot(enigme)
    if DOUBLE_BOT:
        com.expect_peripheral_name = "seraphor"
        print("Send Enigme for bot B")
        send_to_bot(enigme) 
        com.expect_peripheral_name = "Morgator"  
    return enigme

def Mastermind_Manager():
    print("Code : " + str(mastermind_ST.code))
    print("Wait Guess")
    guess = receive_from_bot()
    while guess!= ",".join(map(str,mastermind_ST.code)):
        print("Guess : " + guess)
        responce = mastermind_ST.Send_Responce(guess)
        print("Send Responce : " + responce)
        send_to_bot(responce)
        if responce == "#,#,#,#":
            break
        print("Wait Guess")
        guess = receive_from_bot()
    print("Mastermind solved !")

def State_machine():
    # if FAKE_ST:
    #     send_Path()
    Solve_Path()

    enigme_is_send = False
    while True:
        if FAKE_ST:
            print("Wait state ASK_CUSTOM from bot")
            com.connect_or_exit(retry=100)
            print(com.peripheral_state)
            if com.peripheral_state in ['ASK', 'ASK_CUSTOM']:
                enigme = send_Enigme()
                if 'SUDOKU' in enigme : #TODO sudoku in bot
                    Solve_Enigme()
                    solus = receive_from_bot()
                    print("Solus : "+ solus)
                    if DOUBLE_BOT:
                        com.expect_peripheral_name = "seraphor"
                        print("Wait responce B")
                        solus = receive_from_bot()
                        com.expect_peripheral_name = "Morgator" 
                        print("Solus B : "+ solus)
                elif 'MASTERMIND' in enigme:
                    Mastermind_Manager()
                else:
                    print("Wait Solus from bot")
                    solus = receive_from_bot()
                    print("Solus : "+ solus)
                    if DOUBLE_BOT:
                        com.expect_peripheral_name = "seraphor"
                        print("Wait responce B")
                        solus = receive_from_bot()
                        com.expect_peripheral_name = "Morgator" 
                        print("Solus B : "+ solus)
        else:
            Solve_Enigme()

State_machine()