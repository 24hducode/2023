#!/usr/bin/env python3

import sys

# Detect if we are in simulation mode
if sys.implementation.name == 'cpython':
    print("SIMULATION mode")
    SIMULATION = True
else:
    SIMULATION = False


if SIMULATION:
    from stm32_alphabot_v2_simulation import AlphaBot_v2
    import utime_simulation as utime
    import neopixel_simulation as neopixel
else:
    from stm32_alphabot_v2 import AlphaBot_v2
    import utime
    import neopixel

import _thread

# -------------------------------
# neopixel
# -------------------------------
class FoursNeoPixel():
    def __init__(self, pin_number):
        self._pin = pin_number
        self._max_leds = 4
        self._leds = neopixel.NeoPixel(self._pin, 4)

    def set_led(self, addr, red, green, blue):
        if addr > -1 and addr <= self._max_leds:
            # coded on BGR
            self._leds[addr] = (blue,  green, red)
    def show(self):
        self._leds.write()
    def clear(self):
        for i in range (0, self._max_leds):
            self.set_led(i, 0,0,0)
        self.show()


def neo_french_flag_threaded(leds):
    while True:
        leds.set_led(0, 250, 0, 0)
        leds.set_led(1, 250, 0, 0)
        leds.set_led(2, 250, 0, 0)
        leds.set_led(3, 250, 0, 0)
        leds.show()
        utime.sleep(1)
        leds.set_led(0, 250, 250, 250)
        leds.set_led(1, 250, 250, 250)
        leds.set_led(2, 250, 250, 250)
        leds.set_led(3, 250, 250, 250)
        leds.show()
        utime.sleep(1)
        leds.set_led(0, 0, 0, 250)
        leds.set_led(1, 0, 0, 250)
        leds.set_led(2, 0, 0, 250)
        leds.set_led(3, 0, 0, 250)
        leds.show()
        utime.sleep(1)

        leds.clear()
        utime.sleep(2)

        leds.set_led(0, 0, 250, 0)
        leds.set_led(1, 0, 250, 0)
        leds.set_led(2, 0, 250, 0)
        leds.set_led(3, 0, 250, 0)
        leds.show()
        utime.sleep(1)
        leds.set_led(0, 250, 250, 250)
        leds.set_led(1, 250, 250, 250)
        leds.set_led(2, 250, 250, 250)
        leds.set_led(3, 250, 250, 250)
        leds.show()
        utime.sleep(1)
        leds.set_led(0, 0, 0, 250)
        leds.set_led(1, 0, 0, 250)
        leds.set_led(2, 0, 0, 250)
        leds.set_led(3, 0, 0, 250)
        leds.show()
        utime.sleep(1)

        leds.clear()
        utime.sleep(2)

def neo_french_flag(fours_rgb_leds):
    if SIMULATION:
        neo_french_flag_threaded(fours_rgb_leds)
    else:
        _thread.start_new_thread(neo_french_flag_threaded, ([fours_rgb_leds]))

# -------------------------------
# MAIN
# -------------------------------
alphabot = AlphaBot_v2()

neopixel_leds = FoursNeoPixel(alphabot.pin_RGB)

neo_french_flag(neopixel_leds)