import time

def sleep_us(us):
    time.sleep(float(us/1000000))

def sleep_ms(ms):
    time.sleep(float(ms/1000))

def sleep(s):
    time.sleep(s)