from Peripage import Peripage
import textwrap

SUDOKU = "5,5,8, ,4, ,2, ,6, , , ,3,6, , , , , ,1,4, , ,8, , , , , , , , ,5, , ,8, , ,2, , ,4, , ,9,5, , ,6, , ,3,4, ,2,6,3, ,9, , , , , ,4, , ,2, , , , ,8, , , , ,3, , , ,"
SUDOKU = SUDOKU.replace(" ", "9")
SUDOKU = SUDOKU.replace(",", "|")

txt = "|"+'\n|'.join(textwrap.wrap(SUDOKU, 18))
print(txt)

dico = {0:'Y',1:'B',2:'R',3:'G',4:'W',5:'O'}
toto = "<SUDOKU>abcd"
print(toto[8:])
guess = "Y,Y,B,B"
guess_raw = guess.split(',')

key_list = list(dico.keys())
val_list = list(dico.values())
guess = []
print(guess_raw)
for color in guess_raw:
    position = val_list.index(color)
    guess.append(key_list[position])
print(guess)

words = "['a', 'b', 'c', 'a']"
print(list(words))
#printer = Peripage()
#printer.print_text("ça marche !")
print("ça marche !")

string = '<MYSTERE>D-4:1D-4:1D-4:1G-4:4D#5:4S-1:2C#5:1C-5:1A#4:1A#5:4D#5:2S-1:1C#5:1C-5:1A#4:1A#5:4D#5:2C#5:1C-5:1C#5:1A#4:5'

string = string[9:]
output = [string[i:i+5] for i in range(0, len(string), 5)]
print(output)

