from stm32_vl53l0x import VL53L0X
from stm32_alphabot_v2 import AlphaBot_v2
from stm32_ssd1306 import SSD1306, SSD1306_I2C
from ble_peripheral import DrivePeripheral
import Music_Solver

bot = AlphaBot_v2()
bot.TRSensors_calibrate()
enigme = '<MYSTERE>D-4:1D-4:1D-4:1G-4:4D#5:4S-1:2C#5:1C-5:1A#4:1A#5:4D#5:2S-1:1C#5:1C-5:1A#4:1A#5:4D#5:2C#5:1C-5:1C#5:1A#4:5'
Music_Solver.buzzer_playNotes(bot, enigme)
bot.controlBuzzer(1)