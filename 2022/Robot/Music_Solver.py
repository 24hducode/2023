import utime
import machine

def pitch (bot, noteFrequency, noteDuration, silence_ms = 10):
  if noteFrequency is not 0:
    microsecondsPerWave = 1e6 / noteFrequency
    millisecondsPerCycle = 1000 / (microsecondsPerWave * 2)
    loopTime = noteDuration * millisecondsPerCycle
    for x in range(loopTime):
      bot.controlBuzzer(0)
      utime.sleep_us(int(microsecondsPerWave))
      bot.controlBuzzer(1)
      utime.sleep_us(int(microsecondsPerWave))
  else:
    utime.sleep_ms(int(noteDuration))
  utime.sleep_ms(silence_ms)

def buzzer_playNotes (bot, notes, bpm = 100, ticks = 4):
  pin = machine.Pin('D1', machine.Pin.OUT)
  notes = notes[9:].lower()
  notes = [notes[i:i+5] for i in range(0, len(notes), 5)]
  print(notes)
  NOTE_FREQUENCIES = {
    'c-': 16.352,
    'c#': 17.324, 'db': 17.324,
    'd-': 18.354,
    'd#': 19.445, 'eb': 19.445,
    'e-': 20.602,
    'f-': 21.827,
    'f#': 23.125, 'gb': 23.125,
    'g-': 24.500,
    'g#': 25.957, 'ab': 25.957,
    'a-': 27.500,
    'a#': 29.135, 'bb': 29.135,
    'b-': 30.868,
    's-': 0
  }
  for i in range(len(notes)):
    timeout = 60000 / bpm / ticks
    pin.off()
    n = notes[i].lower()
    data = n.split(':')
    note = 'r'
    octave = 4
    if len(data[0]) > 0:
      lastChar = data[0][-1]
      try:
        octave = int(lastChar)
        note = data[0].replace(lastChar, '')
      except:
        note = data[0]
    noteTicks = 1
    if len(data) > 1: noteTicks = int(data[1])
    n = {
      'note': note,
      'octave': octave,
      'ticks': noteTicks
    }
    n['f'] = NOTE_FREQUENCIES[n['note']]
    for o in range(n['octave']):
      n['f'] = n['f'] * 2
    pitch(bot, n['f'], timeout*n['ticks'])



