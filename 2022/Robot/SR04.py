import machine
import utime

# Ultrasonic TRIG on D3
d3 = machine.Pin('D3', machine.Pin.OUT)
# Ultrasonic ECHO on D2
d2 = machine.Pin('D2', machine.Pin.IN)

def hcsr04_dist():
    trig = d3
    echo = d2
    timeout_us=30000
    trig.off()
    utime.sleep_us(2)
    trig.on()
    utime.sleep_us(10)
    trig.off()
    echo.value()
	
    duration = machine.time_pulse_us(echo, 1, timeout_us)/1e6 # t_echo in seconds
    #sound speed, round-trip/2, get in cm
    dist = 343 * duration/2 * 100
    return dist
