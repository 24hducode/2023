import time

from stm32_vl53l0x import VL53L0X
from stm32_alphabot_v2 import AlphaBot_v2
from stm32_ssd1306 import SSD1306, SSD1306_I2C
from ble_peripheral import DrivePeripheral
import SR04

import Music_Solver
import Mastermind_Solver
import sudoku
import leds

#ST = "ST"
ST = "ST" # Debug fake ST : change with ST
ASK = "ASK" # change with ASK

COEF = [0.015, 0.005, 0, -0.005, -0.015]
SPEED = 14
ROT_SPEED_RIGHT = 15
ROT_SPEED_LEFT = 17
ROT_DELAY = 0.008
NAME = "Morgator"
COM = "COM9"

TOF_DIST = 5 # distance in cm

screen = ["", "", "", ""]

def disp(msg, line=0):
    screen[line] = str(msg)
    # Send to terminal
    print(msg)
    # print on oled screen
    oled.fill(0)
    for i, line_str in enumerate(screen):
        oled.text(line_str, 0, i * 16)
    oled.show()

# Init
bot = AlphaBot_v2()
oled = SSD1306_I2C(128, 64, machine.I2C(1))
vl53l0x = VL53L0X(i2c=machine.I2C(1))
bot.TRSensors_calibrate()
disp(NAME, line = 0)
leds.FrenchFlag()

#Bluetooth
drv = DrivePeripheral(name=NAME)

def follow_line():
    disp("Follow line", line = 1)
    dist = vl53l0x.getRangeMillimeters()/10
    timeStart = time.ticks_ms()
    timeEnd = time.ticks_ms()
    while timeEnd - timeStart < 1000 or dist > TOF_DIST:
        sensors = bot.TRSensors_readLine(0)
        correct = 0
        for i in range(5):
            correct += sensors[i] * COEF[i]
        dist = vl53l0x.getRangeMillimeters()/10
        bot.setMotors(left=SPEED + correct, right = SPEED - correct)
        timeEnd = time.ticks_ms()
    # Follow line Done
    disp("Balise detected", line = 2)
    bot.stop()

def rotate(deg):
    disp("Rotate : "+ str(deg), line = 1)
    if deg > 0: # Rotate right
        disp("RIGHT", line = 2)
        bot.setMotors(left = ROT_SPEED_RIGHT, right = -ROT_SPEED_RIGHT)
    else: # Rotate left
        disp("LEFT", line = 2)
        bot.setMotors(left = -ROT_SPEED_LEFT, right = ROT_SPEED_LEFT)
    time.sleep(abs(deg) * ROT_DELAY)
    bot.stop()

def get_path():
    disp("Path Management ", line = 1)
    #disp("Waiting JSON from ST", line = 2)
    #raw_path = drv.get_message(recipient=ST)
    # Send raw path to computer
    #disp("JSON received\nSend it to PC", line = 2)
    #drv.send_message(recipient='CUSTOM', msg=raw_path)
    # Receive array of orientation (example : [90, -120, 90])
    disp("Waiting path from PC", line = 2)
    path_array =  drv.get_message(recipient="CUSTOM")
    disp("Received Path :"+ str(path_array), line = 2)
    return map(int, path_array.split(' '))

def Enigme_Management():
    disp("Enigme Management", line = 1)
    #disp("Send ASK state", line = 2)
    #drv.send_message(recipient=ST, msg="{'state':"+ASK+"}")
    disp("Waiting Enigme from ST", line = 2)
    enigme = drv.get_message(recipient=ST)
    disp("Enigme : " + enigme, line = 2)
    if '<MYSTERE>' in enigme: #Music
        disp("Play Music", line = 2)
        Music_Solver.buzzer_playNotes(bot, enigme)
        bot.controlBuzzer(1)
        disp("Send mystere:resolu to ST", line = 2)
        drv.send_message(recipient=ST, msg="mystere:resolu")

    elif '<MASTERMIND>' in enigme: #Mastermind
        disp("Send Guess : Y,Y,B,B", line = 2)
        drv.send_message(recipient= ST, msg="Y,Y,B,B") 
        disp("Wait Responce from ST", line = 2)
        responce = drv.get_message(recipient=ST)
        disp("Receive : " + responce, line=2)
        while responce != "#,#,#,#":
            guess = Mastermind_Solver.Get_Guess(responce)
            disp("Send Guess : " + str(guess), line=2)
            drv.send_message(recipient= ST, msg=guess) 
            disp("Wait Responce from ST", line = 2)
            responce = drv.get_message(recipient=ST)
            disp("Receive : " + responce, line=2)
        disp("Mastermind Solved", line=2)

    # elif '<SUDOKU>' in enigme: #SUDOKU
    #     sudoku.resolve_sudoku(drv, ST, enigme[8:])
    else:
        # Send enigme to computer
        disp("Enigme received\nSend it to PC", line = 2)
        drv.send_message(recipient='CUSTOM', msg=enigme)
        # Receive answere
        disp("Waiting answere from PC", line = 2)
        answere =  drv.get_message(recipient=ST)
        disp("Received answere :"+ str(answere), line = 2)
        disp("Send answere to ST", line = 2)
        drv.send_message(recipient=ST, msg=answere)
        disp("Answere send", line = 2)

def State_Machine():
    disp("Start State Machine", line = 1)

    path_array = get_path()
    for orientation in path_array:
        rotate(int(orientation))
        follow_line()
        Enigme_Management()

    # while True:
    #     follow_line()


# Main
#State_Machine()
disp("FIN", line = 1)
disp("Musique ", line = 2)
while(True):
    dist = SR04.hcsr04_dist()
    disp("Dist : " + str(dist), line = 3)
    if (dist < 20):
        leds.TurnOnRGB(255,0,0)
        Music_Solver.pitch(bot, 30+ 5*dist, dist/5, 10)
        bot.setMotors(left=-25, right = -25)
    else:
        bot.stop()
        leds.TurnOnRGB(0,0,255)