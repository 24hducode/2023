import neopixel
import pyb
import utime

# Initialisation des leds
d7 = pyb.Pin('D7', pyb.Pin.OUT)
np_D7 = neopixel.NeoPixel(d7, 4)

def TurnOffLed():
   #choix des couleurs de chaque led
    np_D7[0] = (0, 0, 0)
    np_D7[1] = (0, 0, 0)
    np_D7[2] = (0, 0, 0)
    np_D7[3] = (0, 0, 0)
    #envoie de la commande de leds
    np_D7.write()

def TurnOnRGB(r,g,b):
    #choix des couleurs de chaque led
    np_D7[0] = (r,g,b)
    np_D7[1] = (r,g,b)
    np_D7[2] = (r,g,b)
    np_D7[3] = (r,g,b)
    #envoie de la commande de leds
    np_D7.write()
    
def FrenchFlag ():
    np_D7[0] = (255,0,0)
    np_D7[1] = (255,255,255)
    np_D7[2] = (255,255,255)
    np_D7[3] = (0,0,255)
    #envoie de la commande de leds
    np_D7.write()

def rainbowRGB():
  R = 255
  G = 50
  B = 50
  for G in range(50, 256, 5):
    TurnOnRGB( R, G, B)
    utime.sleep_ms(5)
  for R in range(255, 49, -5):
    TurnOnRGB( R, G, B)
    utime.sleep_ms(5)
  for B in range(50, 256, 5):
    TurnOnRGB( R, G, B)
    utime.sleep_ms(5)
  for G in range(255, 49, -5):
    TurnOnRGB( R, G, B)
    utime.sleep_ms(5)
  for R in range(50, 256, 5):
    TurnOnRGB( R, G, B)
    utime.sleep_ms(5)
  for B in range(255, 49, -5):
    TurnOnRGB( R, G, B)
    utime.sleep_ms(5)