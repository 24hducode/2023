import bluetooth
import struct
import time
from ble_advertising import advertising_payload

# =================
_debug = 1
# ==================


# -----------------------------
# Function
# -----------------------------
def print_debug(level, *var_args):
	if _debug >= level:
		print(*var_args)


# ----------------------------------------
#  BLE Peripheral Management
# ----------------------------------------
_IRQ_CENTRAL_CONNECT = const(1)
_IRQ_CENTRAL_DISCONNECT = const(2)
_IRQ_GATTS_WRITE = const(3)
_IRQ_GATTS_READ_REQUEST = const(4)
_IRQ_SCAN_RESULT = const(5)
_IRQ_SCAN_DONE = const(6)
_IRQ_PERIPHERAL_CONNECT = const(7)
_IRQ_PERIPHERAL_DISCONNECT = const(8)
_IRQ_GATTC_SERVICE_RESULT = const(9)
_IRQ_GATTC_SERVICE_DONE = const(10)
_IRQ_GATTC_CHARACTERISTIC_RESULT = const(11)
_IRQ_GATTC_CHARACTERISTIC_DONE = const(12)
_IRQ_GATTC_DESCRIPTOR_RESULT = const(13)
_IRQ_GATTC_DESCRIPTOR_DONE = const(14)
_IRQ_GATTC_READ_RESULT = const(15)
_IRQ_GATTC_READ_DONE = const(16)
_IRQ_GATTC_WRITE_DONE = const(17)
_IRQ_GATTC_NOTIFY = const(18)
_IRQ_GATTC_INDICATE = const(19)
_IRQ_GATTS_INDICATE_DONE = const(20)
_IRQ_MTU_EXCHANGED = const(21)
_IRQ_L2CAP_ACCEPT = const(22)
_IRQ_L2CAP_CONNECT = const(23)
_IRQ_L2CAP_DISCONNECT = const(24)
_IRQ_L2CAP_RECV = const(25)
_IRQ_L2CAP_SEND_READY = const(26)
_IRQ_CONNECTION_UPDATE = const(27)
_IRQ_ENCRYPTION_UPDATE = const(28)
_IRQ_GET_SECRET = const(29)
_IRQ_SET_SECRET = const(30)

_FLAG_READ = const(0x0002)
_FLAG_WRITE_NO_RESPONSE = const(0x0004)
_FLAG_WRITE = const(0x0008)
_FLAG_NOTIFY = const(0x0010)
_FLAG_INDICATE = const(0x0020)


# ---- Service BLE
_UART_UUID = bluetooth.UUID("6E400001-B5A3-F393-E0A9-E50E24DCCA9E")
# Peripheral use _UART_RX for receive data
_UART_RX = (bluetooth.UUID("6E400002-B5A3-F393-E0A9-E50E24DCCA9E"), _FLAG_WRITE | _FLAG_WRITE_NO_RESPONSE | _FLAG_NOTIFY | _FLAG_INDICATE,)
# Peripheral use _UART_TX for sending data
_UART_TX = (bluetooth.UUID("6E400003-B5A3-F393-E0A9-E50E24DCCA9E"), _FLAG_NOTIFY,)

_UART_SERVICE = (_UART_UUID, (_UART_TX, _UART_RX),)

# ---- Advertising data
# for Advertising
_PROTOCOL_VERSION      = const(0x30)
_DEVICE_ID             = const(0x00)
_24H_PROTOCOL_READY    = const(0x00)
_24H_PROTOCOL_ASK      = const(0x01)
_24H_PROTOCOL_RESULT   = const(0x02)
_24H_PROTOCOL_WAITING = const(0x03)
_24H_PROTOCOL_BUSY     = const(0x04)
_24H_PROTOCOL_ASK_CUSTOM = const(0x05)
_24H_PROTOCOL_RESULT_CUSTOM = const(0x06)
_24H_PROTOCOL_MAX      = const(0x07)

_MANUFACTURER_READY  = struct.pack('>BBB', _PROTOCOL_VERSION, _DEVICE_ID, _24H_PROTOCOL_READY)
_MANUFACTURER_ASK    = struct.pack('>BBB', _PROTOCOL_VERSION, _DEVICE_ID, _24H_PROTOCOL_ASK)
_MANUFACTURER_RESULT = struct.pack('>BBB', _PROTOCOL_VERSION, _DEVICE_ID, _24H_PROTOCOL_RESULT)
_MANUFACTURER_WAITING = struct.pack('>BBB', _PROTOCOL_VERSION, _DEVICE_ID, _24H_PROTOCOL_WAITING)
_MANUFACTURER_BUSY   = struct.pack('>BBB', _PROTOCOL_VERSION, _DEVICE_ID, _24H_PROTOCOL_BUSY)
_MANUFACTURER_ASK_CUSTOM = struct.pack('>BBB', _PROTOCOL_VERSION, _DEVICE_ID, _24H_PROTOCOL_ASK_CUSTOM)
_MANUFACTURER_RESULT_CUSTOM = struct.pack('>BBB', _PROTOCOL_VERSION, _DEVICE_ID, _24H_PROTOCOL_RESULT_CUSTOM)


# MTU Information
_MAX_NB_BYTES = const(256)  # 256 (max MTU negociated by windows) Bytes
global_mtu = _MAX_NB_BYTES - 3  # 256 - 3 (header byte)

# list of recipients
recipients = ('ST', 'CUSTOM')

# Class for managing BLE data
class BLEPeripheralManagement:
	PROTOCOL_STATE_READY   = _24H_PROTOCOL_READY
	PROTOCOL_STATE_ASK     = _24H_PROTOCOL_ASK
	PROTOCOL_STATE_RESULT  = _24H_PROTOCOL_RESULT
	PROTOCOL_STATE_WAITING = _24H_PROTOCOL_WAITING
	PROTOCOL_STATE_BUSY    = _24H_PROTOCOL_BUSY
	PROTOCOL_STATE_ASK_CUSTOM = _24H_PROTOCOL_ASK_CUSTOM
	PROTOCOL_STATE_RESULT_CUSTOM = _24H_PROTOCOL_RESULT_CUSTOM
	_PROTOCOL_STATE_MAX    = _24H_PROTOCOL_MAX

	_STATE_CONNECTED        = 1
	_STATE_DISCONNECTED     = 0

	# NOTE: The name could be changed to be more easily recognize
	def __init__(self, ble, name="AA"):
		self._ble = ble
		self._ble.active(True)
		self._ble.irq(self._irq)

		# management of several characteristics: temperature, humidity, pressure)
		((self._tx_handle, self._rx_handle),) = self._ble.gatts_register_services((_UART_SERVICE,))

		self._connections = set()
		self._callback = None

		self._payload_ready	= advertising_payload(name=name, manufacturer=_MANUFACTURER_READY)
		self._payload_ask	  = advertising_payload(name=name, manufacturer=_MANUFACTURER_ASK)
		self._payload_result   = advertising_payload(name=name, manufacturer=_MANUFACTURER_RESULT)
		self._payload_waiting = advertising_payload(name=name, manufacturer=_MANUFACTURER_WAITING)
		self._payload_busy	 = advertising_payload(name=name, manufacturer=_MANUFACTURER_BUSY)
		self._payload_ask_custom = advertising_payload(name=name, manufacturer=_MANUFACTURER_ASK_CUSTOM)
		self._payload_result_custom = advertising_payload(name=name, manufacturer=_MANUFACTURER_RESULT_CUSTOM)
		self._payload_state	= self.PROTOCOL_STATE_READY
		self._payload = self._payload_ready

		self.state = self._STATE_DISCONNECTED
		self.disconnected_flag = False
		self.mtu_exchanged_flag = False

		# extend the buffer size to read by default (default are 20)
		self._ble.gatts_set_buffer(self._rx_handle, _MAX_NB_BYTES, True)
		self._ble.gatts_set_buffer(self._tx_handle, _MAX_NB_BYTES, True)

		# Display Mac address of BLE object
		dummy, byte_mac = self._ble.config('mac')
		mac_address = ':'.join(['{:02x}'.format(byte_mac[ele]) for ele in range(0, 6)])
		print("%s: MAC Adress: " % (name), mac_address)

	def is_connected(self):
		return self.state == self._STATE_CONNECTED

	def get_protocol_state(self):
		return self.payload_state

	def set_protocol_state(self, state):
		if state >= 0 and state < self._PROTOCOL_STATE_MAX:
			# stop advertising before to update protocol state
			self._ble.gap_advertise(interval_us=None)
			self._payload_state = state
			self._advertise()
			self.mtu_exchanged_flag = False

	# management of BLE events
	def _irq(self, event, data):
		print_debug(2, "Receive IRQ EVENT: ", event)
		# Connect event...
		if event == _IRQ_CENTRAL_CONNECT:
			conn_handle, _, _ = data
			self._connections.add(conn_handle)
			self.state = self._STATE_CONNECTED
			print_debug(1, "[BLE]: Connected, protocol state", self._payload_state)
			# print_debug(1,'TX handle', self._tx_handle, 'RX handle', self._rx_handle)
			# stop advertising
			self._ble.gap_advertise(None)
			self.mtu_exchanged_flag = False

		# Disconnect event...
		elif event == _IRQ_CENTRAL_DISCONNECT:
			conn_handle, _, _ = data
			self._connections.remove(conn_handle)
			self.state = self._STATE_DISCONNECTED
			print_debug(1, "[BLE]: Disconnected")
			self.disconnected_flag = True

		# Write event ...
		elif event == _IRQ_GATTS_WRITE:
			conn_handle, value_handle, = data
			if conn_handle in self._connections and value_handle == self._rx_handle:
				sdata = self._ble.gatts_read(self._rx_handle)
				print_debug(2, "[Receive CONTROL]: ", sdata.decode())
				if self._callback:
					self._callback(sdata.decode())

		# Indicate event validated, send ACK
		elif event == _IRQ_GATTS_INDICATE_DONE:
			conn_handle, value_handle, status = data

		elif event == _IRQ_MTU_EXCHANGED:
			global global_mtu
			# ATT MTU exchange complete (either initiated by us or the remote device).
			conn_handle, mtu = data
			print_debug(1, "[MTU EXCHANGE] ", data)
			global_mtu = mtu
			self.mtu_exchanged_flag = True
		else:
			print_debug(1, "[IRQ] other IRQ", event)

	def set_reception_callback(self, fct):
		self._callback = fct

	def send_data(self, data):
		print_debug(1, "[DEBUG] send tx data: ", [len(data), global_mtu])
		if len(data) > global_mtu:
			chunk_size = global_mtu-3
			for chunk in (data[i: i + chunk_size] for i in range(0, len(data), chunk_size)):
				for conn_handle in self._connections:
					# Notify all connected remote BLE of new controlvalue are available
					self._ble.gatts_notify(conn_handle, self._tx_handle, chunk)
		else:
			for conn_handle in self._connections:
				self._ble.gatts_notify(conn_handle, self._tx_handle, data)

	def _advertise(self, interval_us=500000):
		print_debug(2, "Starting advertising")
		interval_to_use = interval_us
		if self.PROTOCOL_STATE_ASK == self._payload_state:
			print_debug(2, "[DEBUG][_advertise] use payload ask")
			self._payload = self._payload_ask
		elif self.PROTOCOL_STATE_RESULT == self._payload_state:
			print_debug(2, "[DEBUG][_advertise] use payload result")
			self._payload = self._payload_result
		elif self.PROTOCOL_STATE_WAITING == self._payload_state:
			print_debug(2, "[DEBUG][_advertise] use payload waiting")
			self._payload = self._payload_waiting
		elif self.PROTOCOL_STATE_BUSY == self._payload_state:
			print_debug(2, "[DEBUG][_advertise] use payload busy")
			self._payload = self._payload_busy
		elif self.PROTOCOL_STATE_READY == self._payload_state:
			print_debug(2, "[DEBUG][_advertise] use payload ready")
			self._payload = self._payload_ready
		elif self.PROTOCOL_STATE_ASK_CUSTOM == self._payload_state:
			print_debug(2, "[DEBUG][_advertise] use payload ask custom")
			self._payload = self._payload_ask_custom
		elif self.PROTOCOL_STATE_RESULT_CUSTOM == self._payload_state:
			print_debug(2, "[DEBUG][_advertise] use payload result custom")
			self._payload = self._payload_result_custom
		self._ble.gap_advertise(interval_to_use, adv_data=self._payload)

	def close(self):
		for conn_handle in self._connections:
			self._ble.gap_disconnect(conn_handle)
		self._connections.clear()


class DrivePeripheral:

	def __init__(self, name='AA'):
		"""Each peripheral shall have a unique name so when several peripheral are visible to central, central can
		differentiate them and connect on the appropriate peripheral
		:param name: name of this peripheral"""
		ble = bluetooth.BLE()
		self.ble_device = BLEPeripheralManagement(ble, name=name)
		self.data_receive = None
		self.msg_complete_flag = False

		def on_rx(data):
			if self.data_receive is None:
				self.data_receive = data
			else:
				self.data_receive += data
			print_debug(2, "data_receive: ", self.data_receive)
			if self.data_receive.find("<END>") > 0:
				print_debug(2, "On RX: DATA RECEIVE COMPLETE")
				self.msg_complete_flag = True

		self.ble_device.set_reception_callback(on_rx)

	def get_message(self, recipient):
		# get a message sent by ST computer or sent by your own computer
		# :param recipient: use 'ST' for a message to receive from ST computer else use 'CUSTOM'
		# Here are steps to receive a message over BLE, some are managed by this function, others are managed by central:
		# * peripheral changes state so central knows we want to receive a message
		#   note: state to use differs if we want to connect on ST computer (ASK) or on custom computer (ASK_CUSTOM)
		# * central connects to this peripheral (including MTU change)
		# * central send message, message is received by peripheral
		# * central disconnects from this peripheral
		# * peripheral changes state to READY so central do not try to connect on this peripheral till next get or send
		if recipient == 'ST':
			recipient_ask_state = BLEPeripheralManagement.PROTOCOL_STATE_ASK
		elif recipient == 'CUSTOM':
			recipient_ask_state = BLEPeripheralManagement.PROTOCOL_STATE_ASK_CUSTOM
		else:
			print('bad recipient specified')
			return ''
		self.ble_device.set_protocol_state(recipient_ask_state)
		#wait_ticks_left = 200
		while not self.msg_complete_flag:
			time.sleep_ms(100)
			#wait_ticks_left -= 1
			#if not wait_ticks_left:
			#	print_debug(1, "wait too long, reset BLE")
			#	self.ble_device._ble.active(False)
			#	time.sleep_ms(100)
			#	self.ble_device._ble.active(True)
			#	self.ble_device.set_protocol_state(recipient_ask_state)
			#	wait_ticks_left = 200
		self.msg_complete_flag = False
		#print_debug(1, "ticks left", wait_ticks_left)
		msg = self.data_receive[:-5]
		while not self.ble_device.disconnected_flag:
			time.sleep_ms(100)
		self.ble_device.disconnected_flag = False
		self.ble_device.set_protocol_state(BLEPeripheralManagement.PROTOCOL_STATE_READY)
		self.data_receive = None
		return msg

	def send_message(self, recipient, msg):
		# send a message to ST computer, or to your own computer
		# :param recipient: use 'ST' for a message to send to ST computer else use 'CUSTOM'
		# :param msg: text of message
		# Here are steps to send a message over BLE, some are managed by this function, others are managed by central:
		# * peripheral changes state so central knows we want to send a message
		#   note: state to use differs if we want to send to ST computer (RESULT) or to custom computer (RESULT_CUSTOM)
		# * central connects to this peripheral (including MTU change)
		# * peripheral send message, message is received by central
		# * central disconnects from this peripheral
		# * peripheral changes state to READY so central do not try to connect on this peripheral till next send or get
		if recipient == 'ST':
			recipient_result_state = BLEPeripheralManagement.PROTOCOL_STATE_RESULT
		elif recipient == 'CUSTOM':
			recipient_result_state = BLEPeripheralManagement.PROTOCOL_STATE_RESULT_CUSTOM
		else:
			print('bad recipient specified')
			return
		print_debug(3, 'answer ready to send:', self.ble_device.mtu_exchanged_flag)
		self.ble_device.set_protocol_state(recipient_result_state)
		while not self.ble_device.mtu_exchanged_flag:
			time.sleep_ms(100)
		self.ble_device.mtu_exchanged_flag = False
		print_debug(1, 'send answer:', msg)
		self.ble_device.send_data(msg + "<END>")
		while not self.ble_device.disconnected_flag:
			time.sleep_ms(100)
		self.ble_device.disconnected_flag = False
		self.ble_device.set_protocol_state(BLEPeripheralManagement.PROTOCOL_STATE_READY)


# -------------------------------------
# MAIN
# -------------------------------------
if __name__ == '__main__':
	print("BLE Peripheral")
	# init bluetooth
	ble = bluetooth.BLE()
	ble_device = BLEPeripheralManagement(ble, name='AA')
	time.sleep(1)

	data_receive = None
	data_receive_ASK = None
	data_receive_RESULT = None
	data_receive_completed = None
	state = ble_device.PROTOCOL_STATE_ASK

	def on_rx(data):
		print_debug(3, "Receive: ", data)
		global data_receive
		global data_receive_completed
		if data_receive is None:
			data_receive = data
		else:
			data_receive += data
		print_debug(2, "data_receive: ", data_receive)
		if data_receive.find("<END>") > 0:
			data_receive_completed = 1
			print_debug(1, "On RX: DATA RECEIVE COMPLETE")

	ble_device.set_reception_callback(on_rx)

	# set state to ASK
	ble_device.set_protocol_state(ble_device.PROTOCOL_STATE_ASK)
	print("[STATE]: [STATE=ASK]")
	if True:
		# wait connection
		while not ble_device.is_connected():
			time.sleep_ms(200)

		# TODO: put a timeout
		while data_receive_completed is None:
			time.sleep_ms(200)
		data_receive_ASK = data_receive
		data_receive = None

		# wait disconnection
		while ble_device.is_connected():
			time.sleep_ms(200)

		# wait 5 seconds to simulate a processing from peripĥeral
		print("[DEBUG]: Simulate processing by waiting 5 second")
		time.sleep(15)

		data_receive_completed = None

		# set state to RESULT
		ble_device.set_protocol_state(ble_device.PROTOCOL_STATE_RESULT)
		print("[STATE]: [STATE=RESULT]")

		# wait connection
		while not ble_device.is_connected():
			time.sleep_ms(200)

		# send response to ASK
		ble_device.send_data("1234567890"*24 + "<END>")

		# wait disconnection
		while ble_device.is_connected():
			time.sleep_ms(200)

		data_receive_completed = None
		# waiting for a response from server
		ble_device.set_protocol_state(ble_device.PROTOCOL_STATE_WAITING)
		print("[STATE]: [STATE=WAITING]")
		# wait connection
		while not ble_device.is_connected():
			time.sleep_ms(200)

		# TODO: put a timeout
		while data_receive_completed is None:
			time.sleep_ms(200)

		data_receive_RESULT = data_receive

		print("SUMMARY:")
		print("Receive request:\n", data_receive_ASK)
		print("")
		print("Receive response to answer:\n", data_receive_RESULT)
		print("")
